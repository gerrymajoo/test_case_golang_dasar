package main

import (
	"fmt"
)

func main() {
	var a int = 45
	var b *int = &a
	var c int = a

	//change data variabel a
	// we can change from pointer b
	*b = 40
	c = 50

	fmt.Println("value number a : ", a)
	fmt.Println("value number b : ", *b)
	fmt.Println("value number c : ", c)

	// swap
	var x, y int = 1, 2
	x, y = swap(x, y)
	fmt.Println("value number origin x, y : ", x, ",", y)
	fmt.Println("value swap x , y : ", x, ",", y)
}

func swap(x, y int) (int, int) {
	x, y = y, x

	return x, y
}

// perlu function swap yang call by reference
// func swap(x,y *int)
