package main

import (
	"fmt"
	"go/test_case_golang_dasar/entity"
	"go/test_case_golang_dasar/repository"
)

func main() {

	// data dummy
	response := repository.ResponseRepository{
		Response: entity.Response{
			Status:  true,
			Data:    make([]string, 3),
			Message: "test send message",
			Meta: entity.Meta{
				TotalRecord: 20,
				TotalPage:   1,
				Page:        1,
			},
		},
	}

	// set message
	response.SetMessage("test ganti message")

	// test response
	fmt.Println(response)
}
