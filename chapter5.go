package main

import (
	"fmt"
	"go/test_case_golang_dasar/entity"
	"go/test_case_golang_dasar/repository"
)

func main() {

	dataStudent := entity.Student{
		NIS: 12345,
		Human: entity.Human{
			Name:    "Gerry",
			Address: "JL Student Elegant",
			Age:     34,
		},
		KTP: entity.KTP{
			NIK:      4573234513,
			ImageURL: "https://www.instagram.com/p/BwySh0nAH7R/",
		},
	}

	student := repository.NewStudentRepository(dataStudent)
	fmt.Println(student.SayHello())
}
