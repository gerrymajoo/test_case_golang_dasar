package main

import (
	"fmt"
	"go/test_case_golang_dasar/entity"
	"go/test_case_golang_dasar/repository"
	"reflect"
)

func main() {
	response := repository.ResponseRepository{
		Response: entity.Response{
			Status:  true,
			Data:    []string{"India", "Canada", "Japan"},
			Message: "set message",
			Meta: entity.Meta{
				TotalRecord: 10,
				TotalPage:   1,
				Page:        2,
			},
		},
	}

	fmt.Println("")
	fmt.Println("///////////////////////////")
	fmt.Println("Data(interface kosong)")
	fmt.Println("///////////////////////////")

	fmt.Println(response.GetData())
	fmt.Println("var5 = ", reflect.TypeOf(response.GetData()))

	data := response.GetData()

	// print data original
	fmt.Println("data orginal : ", data)

	// check data
	fmt.Println("check data : ", reflect.TypeOf(data))

	// loop for data interface
	for key, city := range data.([]string) {
		fmt.Printf("menampilkan list city :")
		fmt.Printf("city %v : %v\n", key, city)
	}

	fmt.Println("")
	fmt.Println("///////////////////////////")
	fmt.Println("Status (bool), Data(interface kosong), Message(string), Meta (Meta)")
	fmt.Println("///////////////////////////")

	fmt.Println(response)

}
