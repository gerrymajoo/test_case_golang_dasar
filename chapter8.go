package main

import (
	"fmt"
	"go/test_case_golang_dasar/entity"
	"go/test_case_golang_dasar/repository"
)

// Sudah cukup bagus, mungkin nnti bisa di coba tambah explore, bagaimana kalo yang kita kirimkan pada repository itu sebuah interface juga
type Xiomi struct {
	entity.Handphone
}

type Oppo struct {
	entity.Handphone
}

func main() {

	// data dummy xiomy
	xiomi := Xiomi{
		Handphone: entity.Handphone{
			Type:    "gameshack",
			Screen:  "15 inc",
			Color:   "black, white, yellow",
			Variant: 4,
		},
	}

	// data dummy Oppo
	oppo := Oppo{
		Handphone: entity.Handphone{
			Type:    "kmyway87aa",
			Screen:  "20 inc",
			Color:   "magenta, white and black",
			Variant: 2,
		},
	}

	dataXiomi := repository.NewHandphoneRepository(xiomi.Handphone)

	// response xiomi type
	fmt.Println("xiomi type : ", dataXiomi.GetType())

	dataOppo := repository.NewHandphoneRepository(oppo.Handphone)

	// response oppo color
	fmt.Println("oppo color : ", dataOppo.GetColor())
}
