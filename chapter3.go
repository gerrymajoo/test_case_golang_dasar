package main

import (
	"fmt"
)

type Human struct {
	// kalau memakai huruf kecil/private properti, baiknya menyediakan setter dan getter nya (apabila memang diperlukan) untuk pengaksesan diluar package
	name    string
	address string
	age     int
}

// bisa juga apabila memang seluruhnya private, kita bisa membuat new instance dengan function
// func NewHuman()Human{
// 	return Human{}
// }

type Teacher struct {
	NIP int
	Human
	KTP
}

type Student struct {
	NIS int
	Human
	KTP
}

type KTP struct {
	NIK      int
	ImageURL string
}

func main() {

	var human Human = Human{
		"gerry",
		"Jalan",
		15,
	}

	var memoryHuman *Human = &human

	fmt.Println("")
	fmt.Println("///////////////////////////")
	fmt.Println("menyimpan nilainya, dan yg kedua menyimpan alamat memori-nya")
	fmt.Println("///////////////////////////")
	fmt.Println("Original data : ", human)
	fmt.Println("alocation memory data : ", &memoryHuman)

	/**
	 * code for chapter teacher
	 */

	var teacher Teacher = Teacher{
		NIP: 12,
		Human: Human{
			name:    "Mr. gerry",
			address: "JL Rahayu Utama",
			age:     30,
		},
		KTP: KTP{
			NIK:      4573234513,
			ImageURL: "https://www.instagram.com/p/BwySh0nAH7R/",
		},
	}

	var memoryTeacher *Teacher = &teacher

	fmt.Println("")
	fmt.Println("///////////////////////////")
	fmt.Println("instansi object dari Teacher")
	fmt.Println("///////////////////////////")
	fmt.Println("Original data : ", teacher)
	fmt.Println("alocation memory data : ", &memoryTeacher)

	/**
	 * code for chapter teacher
	 */

	var student Student = Student{
		NIS: 12234,
		Human: Human{
			name:    "Mr. gerry",
			address: "JL Rahayu Utama",
			age:     30,
		},
		KTP: KTP{
			NIK:      4573234513,
			ImageURL: "https://www.instagram.com/p/BwySh0nAH7R/",
		},
	}

	var memoryStudent *Student = &student

	fmt.Println("")
	fmt.Println("///////////////////////////")
	fmt.Println("instansi object dari student")
	fmt.Println("///////////////////////////")
	fmt.Println("Original data : ", student)
	fmt.Println("alocation memory data : ", &memoryStudent)

}
