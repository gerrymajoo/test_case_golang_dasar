package entity

type Meta struct {
	TotalRecord int
	TotalPage   int
	Page        int
}
