package entity

type Handphone struct {
	Type    string
	Screen  string
	Color   string
	Variant int
}
