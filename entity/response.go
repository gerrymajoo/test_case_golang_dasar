package entity

type Response struct {
	Status  bool
	Data    interface{}
	Message string
	Meta    Meta
}
