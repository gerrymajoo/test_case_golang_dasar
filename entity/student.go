package entity

type Human struct {
	Name    string
	Address string
	Age     int
}

type Student struct {
	NIS int
	Human
	KTP
}

type KTP struct {
	NIK      int
	ImageURL string
}
