package repository

import (
	"go/test_case_golang_dasar/entity"
)

type ResponseRepository struct {
	entity.Response
}

func NewResponseRepository(response entity.Response) ResponseRepositoryContrack {
	return &ResponseRepository{response}
}

func (response *ResponseRepository) GetStatus() bool {
	return response.Response.Status
}

func (response *ResponseRepository) SetStatus(s bool) {
	response.Response.Status = s
}

func (response *ResponseRepository) GetData() interface{} {
	return response.Response.Data
}

func (response *ResponseRepository) GetMessage() string {
	return response.Response.Message
}

func (response *ResponseRepository) SetMessage(m string) {
	response.Response.Message = m
}

func (response *ResponseRepository) GetPage() int {
	return response.Meta.Page
}

func (response ResponseRepository) SetPage(i int) {
	response.Meta.Page = i
}

func (response ResponseRepository) GetTotalRecord() int {
	return response.Meta.TotalRecord
}

func (response ResponseRepository) GetTotalPage() int {
	return response.Meta.TotalPage
}
