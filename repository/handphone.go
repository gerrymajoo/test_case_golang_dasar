package repository

type HandphoneContract interface {
	GetType() string
	GetScreen() string
	GetColor() string
	GetVariant() int
}
