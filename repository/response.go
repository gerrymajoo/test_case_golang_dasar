package repository

type ResponseRepositoryContrack interface {
	GetStatus() bool
	SetStatus(s bool)
	GetData() interface{}
	GetMessage() string
	SetMessage(m string)
	GetPage() int
	SetPage(i int)
	GetTotalRecord() int
	GetTotalPage() int
}
