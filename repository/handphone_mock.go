package repository

import (
	"go/test_case_golang_dasar/entity"
)

type HandphoneRepository struct {
	entity.Handphone
}

func NewHandphoneRepository(handphone entity.Handphone) HandphoneContract {
	return &HandphoneRepository{handphone}
}

func (handphone *HandphoneRepository) GetType() string {
	return handphone.Type
}

func (handphone *HandphoneRepository) GetScreen() string {
	return handphone.Screen
}

func (handphone *HandphoneRepository) GetColor() string {
	return handphone.Color
}

func (handphone *HandphoneRepository) GetVariant() int {
	return handphone.Variant
}
