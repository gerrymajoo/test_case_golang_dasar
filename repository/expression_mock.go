package repository

import (
	"go/test_case_golang_dasar/entity"
)

func NewStudentRepository(student entity.Student) ExpressionRepository {
	return &Students{student}
}

type Students struct {
	entity.Student
}

func (student *Students) SayHello() string {
	return "Hello Student " + student.Student.Name
}
