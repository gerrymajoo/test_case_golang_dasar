package main

import (
	"fmt"
)

type Student struct {
	NIS int
	Human
	KTP
}

type Human struct {
	name    string
	address string
	age     int
}

type KTP struct {
	NIK      int
	ImageURL string
}

func main() {

	var student Student = Student{
		NIS: 12345,
		Human: Human{
			name:    "Gerry",
			address: "JL Student Elegant",
			age:     34,
		},
		KTP: KTP{
			NIK:      4573234513,
			ImageURL: "https://www.instagram.com/p/BwySh0nAH7R/",
		},
	}

	fmt.Println(student.SayHello())
}

// ini merupakan contoh dari pointer receiver
func (dataStudent *Student) SayHello() string {
	return "Hello Student " + dataStudent.name
}

// contoh value receiver
// func (dataStudent Student) GetName()string

// contoh pointer receiver selanjutnya kita bisa gunakan untuk mengubah suatu nilai
// func (dataStudent *Student) SetName(name string){
// dataStudent.name = name
//}
